#Accepting input(s) from user
name = str(input("Enter Employee Name: "))
id = str(input("Enter Employee Id: "))
basic_salary = float(input("Enter Basic Salary: "))
special_allowances = int(input("Enter monthly Special allowances: "))
percentage_bonus = float(input("Enter Bonus percentage: "))
ts_investment = bool(input("Any Tax saving investment(True)/skip: "))
#Checking if any tax saving investments
if ts_investment:
    x = (basic_salary / 12) + special_allowances
    Annual_salary = x * 12
    Gross_Annual_salary = Annual_salary + ((percentage_bonus * basic_salary) / 100)
    #if true Applying GST or Tax based on conditions
    if basic_salary > (400000) and basic_salary <= (650000):
        GST = (5 * basic_salary) / 100
    elif basic_salary >= (650000) and basic_salary <= (1150000):
        GST = (10 * basic_salary) / 100
    elif basic_salary > (1150000):
        GST = (20 * basic_salary) / 100
    else:
        GST = 0
    Annual_net_salary = Gross_Annual_salary - GST
    #Printing outputs
    print(f"Gross Annual Salary: {Gross_Annual_salary}")
    print(f"Net Annual Salary: {Annual_net_salary}")
    print(f"Tax payable: {GST}")

else:
    x = (basic_salary / 12) + special_allowances
    Annual_salary = x * 12
    Gross_Annual_salary = Annual_salary + ((percentage_bonus * basic_salary) / 100)
    # if true Applying GST or Tax based on conditions
    if basic_salary >= 250000 and basic_salary <= 500000:
        GST = (5 * basic_salary) / 100
    elif basic_salary >= 500000 and basic_salary <= 1000000:
        GST = (10 * basic_salary) / 100
    elif basic_salary > 100000:
        GST = (20 * basic_salary) / 100
    else:
        GST = 0
    Annual_net_salary = Gross_Annual_salary - GST
    #Printing outputs
    print(f"The Annual Salary: {Gross_Annual_salary}")
    print(f"The tax payable: {Annual_net_salary}")
    print(f"Tax payable: {GST}")
