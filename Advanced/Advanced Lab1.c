#include<stdio.h>
#include<conio.h>
void main()
{
    int a[10][10], n, m, i, j;
    printf("Enter no of rows: "); scanf("%d",&n);
    printf("Enter no of columns: "); scanf("%d",&m);
    printf("Enter %d numbers: ",n*m);
    for(i=1;i<=n;i++) for(j=1;j<=m;j++) scanf("%d",&a[i][j]);
    printf("The original matris is: ");
    for(i=1;i<=n;i++) 
    {
        printf("\n");
        for(j=1;j<=m;j++)
        {
            printf("%4d",a[i][j]);
        }
    }
    printf("\nThe Transpose of matri is: ");
    for(i=1;i<=n-1;i++)
    {
        printf("\n");
        for(j=1;j<=m+1;j++)
        {
            printf("%4d",a[j][i]);
        }
    }
}