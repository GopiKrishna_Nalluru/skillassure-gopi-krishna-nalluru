#Accepting input from user
n = int(input("Enter no of elements: "))
arr = []
#Finding the series logic:(i*i)+1
for i in range(n):
    #storing them in an array
    arr.append((i*i)+1)
print(f"The array elements are: ")
print(*arr)
#Finding element with last digit 7 in the array
b = []
for i in arr:
    if i % 10 == 7:
        #storing them in an array
        b.append(i)
print(f"The elements ending with 7 in array are: ")
print(*b)
#Finding the prime numbers among the array
c = []
for i in arr:
    count = 0
    for j in range(1, i+1):
        if i % j == 0:
            count = count+1
    if count == 2:
        c.append(i)
print(f"The prime elements in the array are: ")
print(*c)
#Finding the order of numbers
asc = []
desc = []
no = []
for i in arr:
    c = True
    e = [int(a) for a in str(i)]
    #Checking only for the digits where len of the element in array has 2 or more digits
    if len(e) > 1:
        for x in range(1, len(e)):
            #The last element should be greater than its previous element
            if e[x] >= e[x-1]:
                c = c & True
            else:
                c = c & False
        if c:
            #The elements get stored if it satisfy the condition
            asc.append(i)
d = [i for i in arr if i not in asc]
for i in d:
    c = True
    e = [int(a) for a in str(i)]
    if len(e) > 1:
        for x in range(1, len(e)):
            #The last element should be less than its previous element
            if e[x] <= e[x-1]:
                c = c & True
            else:
               c = c & False
        if c:
            #The elements get stored if it satisfy the condition
            desc.append(i)
        else:
            #The remaining elements stored in an array
            no.append(i)
print(f"The ascending elements in the array are: {asc}\nThe descending elements in the array are: {desc}\nThe no order elements in the array are: {no}")
