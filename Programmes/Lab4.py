#Accepting inputs from users
N = int(input("Enter N value: "))
#Giving initial values
sum = 0
i =1
#applying the condition
while i <= N:
    if i%2 != 0:
        sum = sum+i
    #Increment of var i
    i = i+1
#print the output
print(f"The sum of odd numbers in the list is: {sum}")
