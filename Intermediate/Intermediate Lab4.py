#Initializing the values
m = int(1)
n = int(100)
print(f"The prime numbers between 1 and 100 are: ")
#Using the nested for loop to find the prime numbers between 1 and 100
#Initiating the loop by giving i in range n+1
for i in range(m, n+1):
    count = 0
    #Dividing each number(i) from 1 to i to check prime number
    for j in range(1, i+1):
        if i % j == 0:
            count = count + 1
    #A number divisible by 1(1) and itself(2) is called prime number
    #if i%1==0(1) and i%i==0(2) only satisfies the prime condition
    if count == 2:
        #printig the output
        print(i)
