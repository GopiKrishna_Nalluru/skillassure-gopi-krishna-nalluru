import java.util.Scanner;
public class Main
{
    public static void main(String[] args)
    {
        Scanner input = new Scanner(System.in);
        //initializing the values 
        int a = 1, b = 2, i,c;
        //Accepting the input(s) from user
        System.out.printf("Enter limit: ");
        int n = input.nextInt();
        System.out.println(a);
        System.out.println(b);
        //Using the for loop to get output untill n
        for(i=0;i<n;i++)
        {
            c = a + b;
            System.out.println(c);
            //updating initialized values 
            a = b;
            b = c;
        }
    }
}